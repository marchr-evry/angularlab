# README #

This little repo is for sharing some of the labs from the Angular course that we take together, to help you get started with modifying some code and experimenting by yourself.

### What is this repository for? ###

* Code examples from the course
* Some guides/docs 
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Clone the repo to your local machine, open any of the Level-folders, to see the final code from that Level in the course.


### Any questions? ###

* Come talk to me at my desk. :)
* Email me @ [marcus.christensen@evry.com](mailto:marcus.christensen@evry.com)
* Lync me