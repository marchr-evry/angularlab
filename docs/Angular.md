
#Angular
[Docs](https://docs.angularjs.org/api)

This is a short list of building-blocks used in Angular-apps.
The list is quite 


##Module
A module is used to contain a particular set of functionality
into a simple reusable object.

####Declaration
```javascript
	var app = angular.module('MyModule', []);
```

####Usage
```html
	<html ng-app="MyModule">
		...
	</html>
```



##Controller
A Controller is used to manage the communication between the
model (data) and the view (html).

####Declaration
```javascript
	app.controller('MyController', function() {
	
		this.myProperty = 'A value';
		this.isVisible = true;
	
		// Should only perform actions on data, not on DOM!
		this.myFunction = function() {
			this.myProperty = 'Another value';
			this.isVisible = false;
		};
	});
```

####Usage
```html
	<div ng-controller="MyController as controller">
		{{ controller.myProperty }}
	</div>
```

##Directive
Is used for modifying the DOM in some way, like hiding and showing
a tag, like with the ng-show and ng-hide directives, or repeating
a DOM subtree; stamping out html based on a collection with data.


####Declaration
*We have not seen this yet, it will come soon.*

####Usage
Here are is an example of using the built-in Directives
*ng-show* and *ng-click* in Angular.

```html
	<div ng-controller="MyController as controller">
		...
		<div ng-show="controller.isVisible">
			{{ controller.myProperty }}
		</div>
		...
		<button ng-click="controller.myFunction();">Click me!</button>
		...
	</div>
```

##Filter 
[Builtin](https://docs.angularjs.org/api/ng/filter)
[Docs](https://docs.angularjs.org/api/ng/filter)

A filter is used to alter the presentation of the data in some way,
and is therefore not modyfing the underlaying data structure.


####Declaration
*We have not seen this yet, it will come soon.*

####Usage
	
	{{ data | filter:options }}

```html
	<div ng-show="controller.isVisible">
    	{{ controller.myProperty | filter }}
	</div>
```
