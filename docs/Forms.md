
#Angular Forms

Angular provides some nice functionality to help us with the basic input of data
by the user in a form.



* Best practice is to have a new controller for the form, which makes it really easy
  to validate the data before using it somewhere else.
* If you give the form a name, you can use the **formName.$valid** property to check
  for data-validity before using the data, or performing more checks.
* To bind a value in a form input-tag back to a property on your model, use the
  **ng-model** directive.
* Don't forget to set **novalidate** on the form tag.
* Use **required** on the input-fields you want to be sure is filled.



````html
	<form name="myForm" ng-controller="MyController as controller" novalidate>
		...
		<input type="text" ng-model="controller.myProperty" required/>
		...
		<input type="submit" ng-submit="controller.mySubmitFunction();"/>
		<span>Is form valid: {{ myForm.$valid }}
	</form>
````

###Styling example
* Angular helps us with decorating the inputs with some classes, to let us
  style the form in a simple way.
  * **ng-prestine** / **ng-dirty** - Data is unchanged / changed
  * **ng-invalid** / **ng-valid** - The data is invalid / valid
  
````css
	
	.ng-invalid.ng-dirty {
		border-color: red;
	}
	.ng-valid.ng-dirty {
		border-color: green;
	}

````


##Question
* How do the Angular NumberValidator work with comma (,) and dot (.) in input fields?
  I haven't had the time to look into this yet. Anyone wan't to contribute? :)
